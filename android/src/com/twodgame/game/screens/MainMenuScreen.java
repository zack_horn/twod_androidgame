package com.twodgame.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.twodgame.game.TwoDGame;

public class MainMenuScreen implements Screen {
	
	final TwoDGame game;
	
	private OrthographicCamera camera;

	public MainMenuScreen(final TwoDGame twoDGame) {
		game = twoDGame;
		camera = new OrthographicCamera();
		camera.setToOrtho(false, 800, 480);
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void render(float delta) {
		 Gdx.gl.glClearColor(0, 0, 0.2f, 1);
	     Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

	     camera.update();
	     game.batch.setProjectionMatrix(camera.combined);
	     
	     game.batch.begin();
	     
	     game.font.draw(game.batch, "Later Nerd.", 150, 150);
	     
	     game.batch.end();
	     
	     if(Gdx.input.isTouched()) {
	    	// game.setScreen(screen);
	    	dispose(); 
	     }
	     
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

}
